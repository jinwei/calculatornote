package me.kailai.calculatornote;

import me.kailai.calculatornote.R;
import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.EditText;

import bsh.EvalError;
import bsh.Interpreter;

public class NoteEdit extends Activity
{
	private NotesDbAdapter DbHelper;
	private EditText TitleText;
	private EditText BodyText;
	private Long RowId;
	private static Interpreter BshInterpreter = new Interpreter();

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		DbHelper = new NotesDbAdapter(this);
		DbHelper.open();
		setContentView(R.layout.note_edit);
		setTitle(R.string.edit_note);

		TitleText = (EditText) findViewById(R.id.title);
		BodyText = (EditText) findViewById(R.id.body);

		Button confirmButton = (Button) findViewById(R.id.confirm);

		//检查bundle中是否存有记录数据
		RowId = (savedInstanceState == null) ? null : (Long) savedInstanceState
				.getSerializable(NotesDbAdapter.KEY_ROWID);
		if (RowId == null)
		{
			Bundle extras = getIntent().getExtras();
			RowId = extras != null ? extras.getLong(NotesDbAdapter.KEY_ROWID) : null;
		}
		populateFields();
		
		//Description: 确定按钮其实并没有将记录数据保存到数据库，保存的过程在saveState（）方法中
		confirmButton.setOnClickListener(new View.OnClickListener()
		{
			public void onClick(View view)
			{
				setResult(RESULT_OK);
				finish();
			}
		});
	}
	
    @Override  
    public boolean onKeyUp(int keyCode, KeyEvent event)
    {  
    	if (keyCode == KeyEvent.KEYCODE_ENTER)
    	{  
            String input = BodyText.getText().toString();
            
            //这里用偷懒的方式取用户最后一行输入
            if(input.charAt(0) != '\n')
            	input="\n"+input;//为下一步方便计算
            String lastLine = input.substring(input.lastIndexOf("\n", input.length() - 2));
            
            BodyText.append("运算结果：" + eval(lastLine) + "\n");  
            BodyText.setSelection(BodyText.length());             
        }  
        return true;  
    }  
    
    //用beanshell偷懒来做算式计算  
    private Object eval(String input)
    {  
        try
        {  
            return BshInterpreter.eval(input);  
        }
        catch (EvalError e)
        {  
            return e.getMessage();  
        }  
    } 
    
	private void populateFields()
	{
		if (RowId != null)
		{
			Cursor note = DbHelper.fetchNote(RowId);
			startManagingCursor(note);
			TitleText
					.setText(note.getString(note.getColumnIndexOrThrow(NotesDbAdapter.KEY_TITLE)));
			BodyText.setText(note.getString(note.getColumnIndexOrThrow(NotesDbAdapter.KEY_BODY)));
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
		saveState();
		outState.putSerializable(NotesDbAdapter.KEY_ROWID, RowId);
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		saveState();
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		populateFields();
	}

	private void saveState()
	{
		String title = TitleText.getText().toString();
		String body = BodyText.getText().toString();

		if (RowId == null)
		{
			long id = DbHelper.createNote(title, body);
			if (id > 0)
			{
				RowId = id;
			}
		} else
		{
			DbHelper.updateNote(RowId, title, body);
		}
	}
}
