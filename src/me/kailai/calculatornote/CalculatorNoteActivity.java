package me.kailai.calculatornote;

import me.kailai.calculatornote.R;
import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.AdapterView.AdapterContextMenuInfo;

public class CalculatorNoteActivity extends ListActivity {
	private static final int ACTIVITY_CREATE = 0;
	private static final int ACTIVITY_EDIT = 1;
	private static final int INSERT_ID = Menu.FIRST;
	private static final int DELETE_ID = Menu.FIRST + 1;
	private NotesDbAdapter DbHelper;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.notes_list);
		DbHelper = new NotesDbAdapter(this);
		DbHelper.open();

		fillData();
		registerForContextMenu(getListView());
	}

	private void fillData()
	{
		// 从数据库获取所有的行记录，并且创建item集合用以存放
		Cursor notesCursor = DbHelper.fetchAllNotes();
		startManagingCursor(notesCursor);
		// 从集合里取出每条记录的标题，放入string数组
		String[] from = new String[] { NotesDbAdapter.KEY_TITLE,NotesDbAdapter.KEY_BODY };
		// 用于绑定对应数据显示的布局textview，位于单独的notes_row.xml中
		int[] to = new int[] { R.id.text1,R.id.content };
		// 创建SimpleCursorAdapter，绑定到外观xml文件，参数分别对应数据和具体的layout
		SimpleCursorAdapter notes = new SimpleCursorAdapter(this, R.layout.notes_row, notesCursor, from, to);
		setListAdapter(notes);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{// 重写menu菜单，通过单击menu键触发
		super.onCreateOptionsMenu(menu);
		menu.add(0, INSERT_ID, 0, R.string.menu_insert);
		return true;
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item)
	{
		switch (item.getItemId())
		{
		case INSERT_ID:
			createNote();
			return true;
		}
		return super.onMenuItemSelected(featureId, item);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo)
	{//长按菜单
		super.onCreateContextMenu(menu, v, menuInfo);
		menu.add(0, DELETE_ID, 0, R.string.menu_delete);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
		case DELETE_ID:
			AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
			DbHelper.deleteNote(info.id);
			//删除记录后重新绑定页面的数据
			fillData();
			return true;
		}
		return super.onContextItemSelected(item);
	}

	private void createNote()
	{
		Intent i = new Intent(this, NoteEdit.class);
		startActivityForResult(i, ACTIVITY_CREATE);
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id)
	{
		super.onListItemClick(l, v, position, id);
		Intent i = new Intent(this, NoteEdit.class);
		i.putExtra(NotesDbAdapter.KEY_ROWID, id);
		startActivityForResult(i, ACTIVITY_EDIT);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent)
	{
		super.onActivityResult(requestCode, resultCode, intent);
		fillData();
	}
}